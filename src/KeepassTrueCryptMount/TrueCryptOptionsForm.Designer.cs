﻿namespace KeePassTrueCryptMount
{
    partial class TrueCryptOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTrueCryptExecutable = new System.Windows.Forms.TextBox();
            this.ofdTrueCryptExecutable = new System.Windows.Forms.OpenFileDialog();
            this.lblSeparator = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.chkShowMenuItemAlways = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tipRegistryButton = new System.Windows.Forms.ToolTip(this.components);
            this.btnRegistryResolve = new System.Windows.Forms.Button();
            this.picBanner = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOpenFileDialog = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.llblDonate = new System.Windows.Forms.LinkLabel();
            this.llableHelp = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericAutoTypeTimeout = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.picBanner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAutoTypeTimeout)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "TrueCrypt executable path";
            // 
            // txtTrueCryptExecutable
            // 
            this.txtTrueCryptExecutable.BackColor = System.Drawing.Color.Coral;
            this.txtTrueCryptExecutable.Location = new System.Drawing.Point(6, 105);
            this.txtTrueCryptExecutable.Name = "txtTrueCryptExecutable";
            this.txtTrueCryptExecutable.Size = new System.Drawing.Size(381, 20);
            this.txtTrueCryptExecutable.TabIndex = 12;
            this.txtTrueCryptExecutable.TextChanged += new System.EventHandler(this.OnTrueCryptPathTextChanged);
            // 
            // ofdTrueCryptExecutable
            // 
            this.ofdTrueCryptExecutable.FileName = "TrueCrypt.exe";
            this.ofdTrueCryptExecutable.Filter = "TrueCrypt Executable|TrueCrypt.exe";
            this.ofdTrueCryptExecutable.RestoreDirectory = true;
            // 
            // lblSeparator
            // 
            this.lblSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeparator.Location = new System.Drawing.Point(0, 339);
            this.lblSeparator.Name = "lblSeparator";
            this.lblSeparator.Size = new System.Drawing.Size(481, 2);
            this.lblSeparator.TabIndex = 15;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(393, 344);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(312, 344);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.OnOkButtonClicked);
            // 
            // chkShowMenuItemAlways
            // 
            this.chkShowMenuItemAlways.AutoSize = true;
            this.chkShowMenuItemAlways.Location = new System.Drawing.Point(6, 142);
            this.chkShowMenuItemAlways.Name = "chkShowMenuItemAlways";
            this.chkShowMenuItemAlways.Size = new System.Drawing.Size(299, 17);
            this.chkShowMenuItemAlways.TabIndex = 16;
            this.chkShowMenuItemAlways.Text = "Show TrueCrypt Mount menu item on each KeePass entry";
            this.chkShowMenuItemAlways.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(28, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 91);
            this.label2.TabIndex = 18;
            this.label2.Text = "Notice: It is recommened to uncheck this option after configured your TrueCrypt p" +
    "assword entries, otherwise the mount menu item is visible on each entry, also in" +
    "ternet, TAN and other.";
            // 
            // tipRegistryButton
            // 
            this.tipRegistryButton.IsBalloon = true;
            this.tipRegistryButton.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tipRegistryButton.ToolTipTitle = "Registry read access";
            // 
            // btnRegistryResolve
            // 
            this.btnRegistryResolve.Image = global::KeePassTrueCryptMount.Resources.B16x16_System_Registry;
            this.btnRegistryResolve.Location = new System.Drawing.Point(434, 103);
            this.btnRegistryResolve.Name = "btnRegistryResolve";
            this.btnRegistryResolve.Size = new System.Drawing.Size(35, 23);
            this.btnRegistryResolve.TabIndex = 20;
            this.tipRegistryButton.SetToolTip(this.btnRegistryResolve, "Resolves the TrueCrypt executable from the Registry");
            this.btnRegistryResolve.UseVisualStyleBackColor = true;
            this.btnRegistryResolve.Click += new System.EventHandler(this.OnRegistryResolveButtonClicked);
            // 
            // picBanner
            // 
            this.picBanner.Dock = System.Windows.Forms.DockStyle.Top;
            this.picBanner.Location = new System.Drawing.Point(0, 0);
            this.picBanner.Name = "picBanner";
            this.picBanner.Size = new System.Drawing.Size(480, 70);
            this.picBanner.TabIndex = 19;
            this.picBanner.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::KeePassTrueCryptMount.Resources.MenuItemSample;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.Location = new System.Drawing.Point(235, 165);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(233, 67);
            this.panel1.TabIndex = 17;
            // 
            // btnOpenFileDialog
            // 
            this.btnOpenFileDialog.Image = global::KeePassTrueCryptMount.Resources.B16x16_Folder_Yellow_Open;
            this.btnOpenFileDialog.Location = new System.Drawing.Point(393, 103);
            this.btnOpenFileDialog.Name = "btnOpenFileDialog";
            this.btnOpenFileDialog.Size = new System.Drawing.Size(35, 23);
            this.btnOpenFileDialog.TabIndex = 14;
            this.btnOpenFileDialog.UseVisualStyleBackColor = true;
            this.btnOpenFileDialog.Click += new System.EventHandler(this.OnButtonOpenFileDialogClick);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(28, 243);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(440, 36);
            this.label3.TabIndex = 21;
            this.label3.Text = "By holding the SHIFT key pressed inside of the context menu of the selected entry" +
    ", the mount volume menu item gets also visible.";
            // 
            // llblDonate
            // 
            this.llblDonate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llblDonate.AutoSize = true;
            this.llblDonate.Location = new System.Drawing.Point(3, 349);
            this.llblDonate.Name = "llblDonate";
            this.llblDonate.Size = new System.Drawing.Size(42, 13);
            this.llblDonate.TabIndex = 22;
            this.llblDonate.TabStop = true;
            this.llblDonate.Text = "Donate";
            this.llblDonate.VisitedLinkColor = System.Drawing.Color.Blue;
            this.llblDonate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.OnDonateLabelLinkClicked);
            // 
            // llableHelp
            // 
            this.llableHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llableHelp.AutoSize = true;
            this.llableHelp.Location = new System.Drawing.Point(80, 349);
            this.llableHelp.Name = "llableHelp";
            this.llableHelp.Size = new System.Drawing.Size(29, 13);
            this.llableHelp.TabIndex = 23;
            this.llableHelp.TabStop = true;
            this.llableHelp.Text = "Help";
            this.llableHelp.VisitedLinkColor = System.Drawing.Color.Blue;
            this.llableHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.OnHelpLabelLinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 279);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "AutoType Execution Timeout";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(91, 298);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "ms";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(168, 295);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(300, 26);
            this.label6.TabIndex = 28;
            this.label6.Text = "On slower computers or containers on external network drives,\r\nit is recommed to " +
    "increase this timeout.";
            // 
            // numericAutoTypeTimeout
            // 
            this.numericAutoTypeTimeout.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericAutoTypeTimeout.Location = new System.Drawing.Point(6, 296);
            this.numericAutoTypeTimeout.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.numericAutoTypeTimeout.Minimum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.numericAutoTypeTimeout.Name = "numericAutoTypeTimeout";
            this.numericAutoTypeTimeout.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericAutoTypeTimeout.Size = new System.Drawing.Size(79, 20);
            this.numericAutoTypeTimeout.TabIndex = 29;
            this.numericAutoTypeTimeout.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // TrueCryptOptionsForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(480, 374);
            this.Controls.Add(this.numericAutoTypeTimeout);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.llableHelp);
            this.Controls.Add(this.llblDonate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnRegistryResolve);
            this.Controls.Add(this.picBanner);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chkShowMenuItemAlways);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblSeparator);
            this.Controls.Add(this.btnOpenFileDialog);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTrueCryptExecutable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "TrueCryptOptionsForm";
            this.ShowInTaskbar = false;
            this.Text = "TrueCryptOptionsForm";
            ((System.ComponentModel.ISupportInitialize)(this.picBanner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAutoTypeTimeout)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpenFileDialog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTrueCryptExecutable;
        private System.Windows.Forms.OpenFileDialog ofdTrueCryptExecutable;
        private System.Windows.Forms.Label lblSeparator;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.CheckBox chkShowMenuItemAlways;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox picBanner;
        private System.Windows.Forms.Button btnRegistryResolve;
        private System.Windows.Forms.ToolTip tipRegistryButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel llblDonate;
        private System.Windows.Forms.LinkLabel llableHelp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericAutoTypeTimeout;
    }
}