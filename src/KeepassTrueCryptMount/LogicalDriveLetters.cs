﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassTrueCryptMount
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    internal static class LogicalDriveLetters
    {
        static LogicalDriveLetters()
        {
            AllDriveLetters = new[]
                                  {
                                      "C:\\", "D:\\", "E:\\", "F:\\", "G:\\", "H:\\", "I:\\", "J:\\",
                                      "L:\\", "M:\\", "N:\\", "O:\\", "P:\\", "Q:\\", "R:\\", "S:\\",
                                      "T:\\", "U:\\", "V:\\", "W:\\", "X:\\", "Y:\\", "Z:\\",
                                  };
        }

        /// <summary>
        /// Gets all drive letters.
        /// </summary>
        /// <value>All drive letters.</value>
        public static IEnumerable<string> AllDriveLetters
        {
            get; 
            private set;
        }

        /// <summary>
        /// Gets the free drive letters.
        /// </summary>
        /// <value>The free drive letters.</value>
        public static IEnumerable<string> FreeDriveLetters
        {
            get
            {
                string[] usedDriveLetters = null;

                try
                {
                    usedDriveLetters = DriveInfo.GetDrives().Select(di => di.Name).ToArray();
                }
                catch (IOException)
                {
                }
                catch(UnauthorizedAccessException) 
                {
                }

                return usedDriveLetters == null ? AllDriveLetters : AllDriveLetters.Except(usedDriveLetters);
            }
        }

        /// <summary>
        /// Ensures the specified string contains drive letter.
        /// </summary>
        /// <param name="letter">The drive letter containing string.</param>
        /// <returns></returns>
        public static string EnsureIsDrive(this string letter)
        {
            if (string.IsNullOrEmpty(letter))
            {
                return string.Empty;
            }

            var drive = letter[0] + ":\\";

            if (AllDriveLetters.Contains(drive))
            {
                return drive;
            }

            return string.Empty;
        }
    }
}
