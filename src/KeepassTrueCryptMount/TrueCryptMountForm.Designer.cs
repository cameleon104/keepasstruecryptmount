﻿namespace KeePassTrueCryptMount
{
    partial class TrueCryptMountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bannerPanel = new System.Windows.Forms.Panel();
            this.m_lblSeparator = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.mountOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.mountOptionsLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.readonlyCheckBox = new System.Windows.Forms.CheckBox();
            this.removableCheckBox = new System.Windows.Forms.CheckBox();
            this.backgroundCheckBox = new System.Windows.Forms.CheckBox();
            this.silentCheckBox = new System.Windows.Forms.CheckBox();
            this.beepCheckBox = new System.Windows.Forms.CheckBox();
            this.explorerCheckBox = new System.Windows.Forms.CheckBox();
            this.askPasswordCheckBox = new System.Windows.Forms.CheckBox();
            this.volumeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.driveComboBox = new System.Windows.Forms.ComboBox();
            this.tipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.btnReloadDrives = new System.Windows.Forms.Button();
            this.hideDialogCheckBox = new System.Windows.Forms.CheckBox();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stateToolStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnApply = new System.Windows.Forms.Button();
            this.tipWarning = new System.Windows.Forms.ToolTip(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.keyFilesTextBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.mountOptionsGroupBox.SuspendLayout();
            this.mountOptionsLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // bannerPanel
            // 
            this.bannerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.bannerPanel.Location = new System.Drawing.Point(0, 0);
            this.bannerPanel.Name = "bannerPanel";
            this.bannerPanel.Size = new System.Drawing.Size(480, 70);
            this.bannerPanel.TabIndex = 0;
            // 
            // m_lblSeparator
            // 
            this.m_lblSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_lblSeparator.Location = new System.Drawing.Point(3, 322);
            this.m_lblSeparator.Name = "m_lblSeparator";
            this.m_lblSeparator.Size = new System.Drawing.Size(475, 2);
            this.m_lblSeparator.TabIndex = 7;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(396, 327);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(315, 327);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "&Mount";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // mountOptionsGroupBox
            // 
            this.mountOptionsGroupBox.Controls.Add(this.mountOptionsLayoutPanel);
            this.mountOptionsGroupBox.Location = new System.Drawing.Point(3, 233);
            this.mountOptionsGroupBox.Name = "mountOptionsGroupBox";
            this.mountOptionsGroupBox.Size = new System.Drawing.Size(468, 79);
            this.mountOptionsGroupBox.TabIndex = 8;
            this.mountOptionsGroupBox.TabStop = false;
            this.mountOptionsGroupBox.Text = "Mount options";
            // 
            // mountOptionsLayoutPanel
            // 
            this.mountOptionsLayoutPanel.Controls.Add(this.readonlyCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.removableCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.backgroundCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.silentCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.beepCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.explorerCheckBox);
            this.mountOptionsLayoutPanel.Controls.Add(this.askPasswordCheckBox);
            this.mountOptionsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mountOptionsLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.mountOptionsLayoutPanel.Name = "mountOptionsLayoutPanel";
            this.mountOptionsLayoutPanel.Size = new System.Drawing.Size(462, 60);
            this.mountOptionsLayoutPanel.TabIndex = 0;
            // 
            // readonlyCheckBox
            // 
            this.readonlyCheckBox.Location = new System.Drawing.Point(3, 3);
            this.readonlyCheckBox.Name = "readonlyCheckBox";
            this.readonlyCheckBox.Size = new System.Drawing.Size(100, 24);
            this.readonlyCheckBox.TabIndex = 0;
            this.readonlyCheckBox.Text = "Readonly";
            this.tipInfo.SetToolTip(this.readonlyCheckBox, "Mount volume as read-only. ");
            this.readonlyCheckBox.UseVisualStyleBackColor = true;
            this.readonlyCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // removableCheckBox
            // 
            this.removableCheckBox.Location = new System.Drawing.Point(109, 3);
            this.removableCheckBox.Name = "removableCheckBox";
            this.removableCheckBox.Size = new System.Drawing.Size(100, 24);
            this.removableCheckBox.TabIndex = 1;
            this.removableCheckBox.Text = "Removable";
            this.tipInfo.SetToolTip(this.removableCheckBox, "Mount volume as removable medium.");
            this.removableCheckBox.UseVisualStyleBackColor = true;
            this.removableCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // backgroundCheckBox
            // 
            this.backgroundCheckBox.Location = new System.Drawing.Point(215, 3);
            this.backgroundCheckBox.Name = "backgroundCheckBox";
            this.backgroundCheckBox.Size = new System.Drawing.Size(100, 24);
            this.backgroundCheckBox.TabIndex = 4;
            this.backgroundCheckBox.Text = "Background";
            this.tipInfo.SetToolTip(this.backgroundCheckBox, "Automatically perform requested actions and exit (main TrueCrypt window will not " +
        "be displayed).");
            this.backgroundCheckBox.UseVisualStyleBackColor = true;
            this.backgroundCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // silentCheckBox
            // 
            this.silentCheckBox.Location = new System.Drawing.Point(321, 3);
            this.silentCheckBox.Name = "silentCheckBox";
            this.silentCheckBox.Size = new System.Drawing.Size(100, 24);
            this.silentCheckBox.TabIndex = 2;
            this.silentCheckBox.Text = "Silent";
            this.tipInfo.SetToolTip(this.silentCheckBox, "Suppresses interaction with the user (prompts, error messages, warnings, etc.).");
            this.silentCheckBox.UseVisualStyleBackColor = true;
            this.silentCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // beepCheckBox
            // 
            this.beepCheckBox.Location = new System.Drawing.Point(3, 33);
            this.beepCheckBox.Name = "beepCheckBox";
            this.beepCheckBox.Size = new System.Drawing.Size(100, 24);
            this.beepCheckBox.TabIndex = 3;
            this.beepCheckBox.Text = "Beep";
            this.tipInfo.SetToolTip(this.beepCheckBox, "Beep after a volume has been successfully mounted or dismounted. ");
            this.beepCheckBox.UseVisualStyleBackColor = true;
            this.beepCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // explorerCheckBox
            // 
            this.explorerCheckBox.Location = new System.Drawing.Point(109, 33);
            this.explorerCheckBox.Name = "explorerCheckBox";
            this.explorerCheckBox.Size = new System.Drawing.Size(100, 24);
            this.explorerCheckBox.TabIndex = 5;
            this.explorerCheckBox.Text = "Open Explorer";
            this.tipInfo.SetToolTip(this.explorerCheckBox, "Open an Explorer window after a volume has been mounted.");
            this.explorerCheckBox.UseVisualStyleBackColor = true;
            this.explorerCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // askPasswordCheckBox
            // 
            this.askPasswordCheckBox.Location = new System.Drawing.Point(215, 33);
            this.askPasswordCheckBox.Name = "askPasswordCheckBox";
            this.askPasswordCheckBox.Size = new System.Drawing.Size(100, 24);
            this.askPasswordCheckBox.TabIndex = 6;
            this.askPasswordCheckBox.Text = "Ask password";
            this.tipInfo.SetToolTip(this.askPasswordCheckBox, "By unchecking this option the volume gets mounted with help of the AutoType funct" +
        "ion \r\nwith enabled \"Two-Channel Auto-Type Obfuscation\".");
            this.askPasswordCheckBox.UseVisualStyleBackColor = true;
            this.askPasswordCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // volumeTextBox
            // 
            this.volumeTextBox.Location = new System.Drawing.Point(6, 105);
            this.volumeTextBox.Name = "volumeTextBox";
            this.volumeTextBox.Size = new System.Drawing.Size(421, 20);
            this.volumeTextBox.TabIndex = 9;
            this.volumeTextBox.TextChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Volume File | Partition | Harddrive";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Drive letter";
            // 
            // driveComboBox
            // 
            this.driveComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.driveComboBox.FormattingEnabled = true;
            this.driveComboBox.Location = new System.Drawing.Point(9, 197);
            this.driveComboBox.Name = "driveComboBox";
            this.driveComboBox.Size = new System.Drawing.Size(121, 21);
            this.driveComboBox.TabIndex = 13;
            this.driveComboBox.SelectionChangeCommitted += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // tipInfo
            // 
            this.tipInfo.AutoPopDelay = 30000;
            this.tipInfo.InitialDelay = 500;
            this.tipInfo.IsBalloon = true;
            this.tipInfo.ReshowDelay = 1000;
            this.tipInfo.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tipInfo.ToolTipTitle = "Helpful information";
            // 
            // btnReloadDrives
            // 
            this.btnReloadDrives.BackgroundImage = global::KeePassTrueCryptMount.Resources.B16x16_Button_Reload;
            this.btnReloadDrives.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnReloadDrives.Location = new System.Drawing.Point(136, 195);
            this.btnReloadDrives.Name = "btnReloadDrives";
            this.btnReloadDrives.Size = new System.Drawing.Size(35, 24);
            this.btnReloadDrives.TabIndex = 17;
            this.tipInfo.SetToolTip(this.btnReloadDrives, "Refresh free drive letters selection");
            this.btnReloadDrives.UseVisualStyleBackColor = true;
            this.btnReloadDrives.Click += new System.EventHandler(this.OnReloadDrivesButtonClicked);
            // 
            // hideDialogCheckBox
            // 
            this.hideDialogCheckBox.AutoSize = true;
            this.hideDialogCheckBox.Location = new System.Drawing.Point(97, 331);
            this.hideDialogCheckBox.Name = "hideDialogCheckBox";
            this.hideDialogCheckBox.Size = new System.Drawing.Size(124, 17);
            this.hideDialogCheckBox.TabIndex = 16;
            this.hideDialogCheckBox.Text = "Hide dialog next time";
            this.tipInfo.SetToolTip(this.hideDialogCheckBox, "By check this option the next mount operation starts without\r\nshowing this dialog" +
        ". To show it again, hold the SHIFT key by\r\nclick the \"Mount volume\" menu item.");
            this.hideDialogCheckBox.UseVisualStyleBackColor = true;
            this.hideDialogCheckBox.CheckedChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Status:";
            // 
            // stateToolStripLabel
            // 
            this.stateToolStripLabel.Name = "stateToolStripLabel";
            this.stateToolStripLabel.Size = new System.Drawing.Size(49, 17);
            this.stateToolStripLabel.Text = "<State>";
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(9, 327);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(80, 23);
            this.btnApply.TabIndex = 15;
            this.btnApply.Text = "&Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            // 
            // tipWarning
            // 
            this.tipWarning.AutoPopDelay = 30000;
            this.tipWarning.InitialDelay = 500;
            this.tipWarning.IsBalloon = true;
            this.tipWarning.ReshowDelay = 1000;
            this.tipWarning.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.tipWarning.ToolTipTitle = "Security issue warning";
            // 
            // button1
            // 
            this.button1.Image = global::KeePassTrueCryptMount.Resources.B16x16_Folder_Yellow_Open;
            this.button1.Location = new System.Drawing.Point(433, 102);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 24);
            this.button1.TabIndex = 11;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnFileOpenDialogButtonClicked);
            // 
            // keyFilesTextBox
            // 
            this.keyFilesTextBox.Location = new System.Drawing.Point(6, 150);
            this.keyFilesTextBox.Name = "keyFilesTextBox";
            this.keyFilesTextBox.Size = new System.Drawing.Size(421, 20);
            this.keyFilesTextBox.TabIndex = 18;
            this.keyFilesTextBox.TextChanged += new System.EventHandler(this.OnAnyOptionChanged);
            // 
            // button2
            // 
            this.button2.Image = global::KeePassTrueCryptMount.Resources.B16x16_Folder_Yellow_Open;
            this.button2.Location = new System.Drawing.Point(433, 147);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(35, 24);
            this.button2.TabIndex = 19;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnFileOpenDialogButtonClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "KeyFile(s)";
            // 
            // TrueCryptMountForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 359);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.keyFilesTextBox);
            this.Controls.Add(this.btnReloadDrives);
            this.Controls.Add(this.hideDialogCheckBox);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.driveComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.volumeTextBox);
            this.Controls.Add(this.mountOptionsGroupBox);
            this.Controls.Add(this.m_lblSeparator);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.bannerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TrueCryptMountForm";
            this.Text = "TrueCryptMountForm";
            this.mountOptionsGroupBox.ResumeLayout(false);
            this.mountOptionsLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel bannerPanel;
        private System.Windows.Forms.Label m_lblSeparator;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox mountOptionsGroupBox;
        private System.Windows.Forms.FlowLayoutPanel mountOptionsLayoutPanel;
        private System.Windows.Forms.CheckBox readonlyCheckBox;
        private System.Windows.Forms.CheckBox removableCheckBox;
        private System.Windows.Forms.TextBox volumeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox driveComboBox;
        private System.Windows.Forms.ToolTip tipInfo;
        private System.Windows.Forms.CheckBox silentCheckBox;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel stateToolStripLabel;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.CheckBox hideDialogCheckBox;
        private System.Windows.Forms.CheckBox beepCheckBox;
        private System.Windows.Forms.CheckBox backgroundCheckBox;
        private System.Windows.Forms.CheckBox explorerCheckBox;
        private System.Windows.Forms.CheckBox askPasswordCheckBox;
        private System.Windows.Forms.Button btnReloadDrives;
        private System.Windows.Forms.ToolTip tipWarning;
        private System.Windows.Forms.TextBox keyFilesTextBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
    }
}