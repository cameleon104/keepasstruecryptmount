﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeePassTrueCryptMount
{
    public partial class TrueCryptMountForm
    {
        internal class AutoDriveLetterComboBoxItem
        {
            public override string ToString()
            {
                return "<Auto Select>";
            }
        }
    }
}
