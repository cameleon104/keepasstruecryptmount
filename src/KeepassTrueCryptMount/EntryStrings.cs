﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassTrueCryptMount
{
    internal static class EntryStrings
    {
        public const string Enabled = "truecrypt.enabled";

        public const string DriveLetter = "truecrypt.drive";

        public const string Volume = "truecrypt.container";

        public const string KeyFiles = "truecrypt.keyfiles";

        public const string Readonly = "truecrypt.option.readonly";

        public const string Removable = "truecrypt.option.removable";

        public const string Silent = "truecrypt.option.silent";

        public const string Background = "truecrypt.option.quit";

        public const string Beep = "truecrypt.option.beep";

        public const string Explorer = "truecrypt.option.explorer";

        public const string MountWithoutDialog = "truecrypt.mount.without.dialog";

        public const string AskForPassword = "truecrypt.option.askpassword";
    }
}
